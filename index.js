// function prompt full name age loc
//

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

let userFullName, userAge, userLoc;

function getUserInfo(){
    userFullName = prompt("What is your name?");
    userAge = prompt("How old are you?");
    userLoc = prompt("Where do you live?");
    alert("Thank you for your input!");
    console.log(`Hello, ${userFullName}`);
    console.log(`You are ${userAge} years old.`);
    console.log(`You live in ${userLoc}`);
}

getUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
let bands =["1. The Beatles", "2.Metallica", "3. The Eagles", "4. L'arc~en~Ciel", "5. EraserHeads"] ;
function topMusicBands(){
	for (let i=0; i<5; i++){
		console.log(`${bands[i]}`);
	}
}
topMusicBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
let movies = ["1. The Godfather", "2.The Godfather, Part II", "3. Shawshank Redemption", "4. To Kill A MockingBird", "5. Psycho"];
let movieRatings = ["Rotten Tomatoes Rating: 97%", "Rotten Tomatoes Rating: 96%", "Rotten Tomatoes Rating: 91%", "Rotten Tomatoes Rating: 93%", "Rotten Tomatoes Rating: 96%", ];

function topMovies(){
	for (let i=0; i<5; i++){
		console.log(`${movies[i]}`);
		console.log(`${movieRatings[i]}`);
	}
}

topMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();